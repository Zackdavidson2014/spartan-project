﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spartan.Model
{
    public class EquipmentType : EquipmentBase
    {
        public string Description { get; set; }
    }
}
