﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spartan.Model
{

    /// <summary>
    /// Equipment class defines a new equipment type
    /// </summary>
    public class Equipment: EquipmentBase
    {
        public string EquipmentTypeId { get; set; }
        public int MeterReading { get; set; }
    }
}
