﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spartan.Model
{

    /// <summary>
    /// Main base class to support equipment
    /// </summary>
    public abstract class EquipmentBase
    {
        public string Id { get; set; }
        public string ExternalId { get; set; }
    }
}
