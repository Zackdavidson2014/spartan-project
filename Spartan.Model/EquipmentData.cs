﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spartan.Model
{
    public class EquipmentData
    {
        public List<Equipment> SerialisedEquipment { get; set; }
        public List<EquipmentType> EquipmentType { get; set; }
    }
}
