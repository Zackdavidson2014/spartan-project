﻿using Spartan.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spartan.Core.Interfaces
{

    /// <summary>
    /// Generic finding manager
    /// </summary>
    /// <typeparam name="Type"></typeparam>
    public interface IManager<Type>
        where Type : EquipmentBase
    {
        List<Type> GetByUnitNumber(string uniqueId);
        List<Type> GetByEquipmentType(string equipmentTypeId);
        List<Type> GetAll();
    }
}
