﻿using Spartan.Model;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using System.Reflection;
using System;
using Spartan.Core.Interfaces;

namespace Spartan.Core.Managers
{
    /// <summary>
    /// A sealed equipment manager class that handles anything related equipments, ie loading equipment.
    /// Access this class through <see cref="Get()"/>
    /// </summary>
    public sealed class EquipmentManager: IManager<EquipmentBase>
    {

        /// <summary>
        /// All of the equipment data
        /// </summary>
        public List<EquipmentBase> Data { get; set; }
        
        /// <summary>
        /// Gets equipment by unit number
        /// </summary>
        /// <param name="uniqueId">the unique id of the equipment</param>
        /// <returns>a list of all equipment</returns>
        public List<EquipmentBase> GetByUnitNumber(string uniqueId)
        {
            List<EquipmentBase> found = new List<EquipmentBase>();
            foreach (var serialised in Data)
            {
                if (serialised.Id.ToLower().Contains(uniqueId.ToLower()))
                {
                    found.Add(serialised);
                }
            }
            return found;
        }

        /// <summary>
        /// Gets equipment by equipment type id, not implmented currently!
        /// </summary>
        /// <param name="equipmentTypeId"></param>
        /// <returns>a list of all equipment with the equipment type id</returns>
        public List<EquipmentBase> GetByEquipmentType(string equipmentTypeId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets all the equipment, i technically would have pagination but time restrictions
        /// </summary>
        /// <returns>a list of all equipment</returns>
        public List<EquipmentBase> GetAll()
        {
            return Data;
        }


        /// <summary>
        /// Loads the equipment to the <see cref="Data"/> property
        /// </summary>
        public void LoadEquipment()
        {
            string content = ReadEquipmentFile();
            var data = JsonConvert.DeserializeObject<EquipmentData>(content);

            List<EquipmentBase> listOfData = new List<EquipmentBase>();
            listOfData.AddRange(data.EquipmentType);
            listOfData.AddRange(data.SerialisedEquipment);

            Data = listOfData;
        }

        /// <summary>
        /// Reads the EquipmentData.json file in the Data folder
        /// </summary>
        /// <returns>The content of EquipmentFile</returns>
        private string ReadEquipmentFile()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "Data/EquipmentData.json";
            return File.ReadAllText(path);
        }


        #region Singleton Pattern
        private static EquipmentManager instance = new EquipmentManager();

        /// <summary>
        /// Gets the singleton instance
        /// </summary>
        /// <returns>The singleton EquipmentManager class</returns>
        public static EquipmentManager Get()
        {
            return instance;
        }

        #endregion

        /// <summary>
        /// Private constructor to stop initialization of <see cref="EquipmentManager"/> anywhere
        /// </summary>
        private EquipmentManager() { }

    }
}
