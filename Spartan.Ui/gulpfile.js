var gulp = require('gulp');
var sass = require('gulp-ruby-sass');
var minifyCSS = require('gulp-csso');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('html', function () {
    return gulp.src('src/*.html')
        .pipe(gulp.dest('dist/'))
});

gulp.task('sass', () =>
    sass('src/scss/**/*.scss')
        .on('error', sass.logError)
        .pipe(gulp.dest('dist/')));

gulp.task('copy-img', () => {
    return gulp.src('src/img/**/*.*')
        .pipe(gulp.dest('dist/img'));
}
);

gulp.task('js', function () {
    return gulp.src(
        [
            'node_modules/jquery/dist/jquery.min.js',
            'node_modules/bootstrap/dist/js/bootstrap.min.js',
            'src/js/*.js',
        ]
    )
        .pipe(sourcemaps.init())
        .pipe(concat('app.min.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/'))
});

gulp.task('default', ['html', 'sass', 'js', 'copy-img']);