/**
 * Item template for populating the results table
 */
const itemTemplate = `
    <div class="entry">
        <div class="title">{{0}}</div>
        <div class="other-amount">{{1}}</div>
        <div class="description">{{2}}</div>
        <div class="image"><img src="./img/arrow-point-to-right.svg" /></div>
    </div>
`;

const noItemsFound = `
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <strong>No Results Found!</strong> Try refining your search more
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>

`;

//Main url for rest end point
const mainUrl = 'http://localhost:52045/api/';
const enterKey = 13;
var foundSearchItems = [];

/**
 * Creates a new item using the template
 */
function createNewItem(title, other, desc) {
    return itemTemplate.replace("{{0}}", title)
        .replace("{{1}}", other)
        .replace("{{2}}", desc);
}

function getAll() {
    $.get(mainUrl + "/equipment/get", function (data) {
        foundSearchItems = data;
        updateUi();
    });
}
getAll();

function handleKeyPress(event) {
    if (event.keyCode === enterKey) {
        search();
    }
}

function search() {
    var stringTerm = $('#search-text').val();
    if (stringTerm === undefined || stringTerm === '') {
        getAll();
        return;
    }
    $.get(mainUrl + "/equipment/GetByUniqueId?uniqueId=" + stringTerm, function (data) {
        foundSearchItems = data;
        updateUi();
    });
}

function updateUi() {
    var mainText = '';
    $('#result-count').text(foundSearchItems.length);
    if (foundSearchItems.length === 0) {
        $('#search-results').html(noItemsFound);
        return;
    }
    foundSearchItems.forEach(element => {
        if (element !== undefined)
            mainText += createNewItem(element['Id'], element['ExternalId'], element['Description']);
    });
    $('#search-results').html(mainText);
};