﻿using Spartan.Core.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Spartan.WebApi.Controllers
{

    /// <summary>
    /// Handles the requests to the equipment controller
    /// </summary>
    public class EquipmentController : ApiController
    {

        /// <summary>
        /// Gets all the equipment
        /// </summary>
        /// <returns></returns>
        public IEnumerable<dynamic> Get()
        {
            return EquipmentManager.Get().GetAll();
        }
        
        public dynamic GetByUniqueId(string uniqueId)
        {
            if (uniqueId == null)
            {
                return Request.CreateErrorResponse(
                    HttpStatusCode.BadRequest, 
                    new HttpError("uniqueId can't be null"));
            }
            return EquipmentManager.Get().GetByUnitNumber(uniqueId);
        }
        
    }
}